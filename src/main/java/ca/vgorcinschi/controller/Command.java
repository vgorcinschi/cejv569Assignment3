package ca.vgorcinschi.controller;

/**
 * the command part of the mediator pattern
 * @author vgorcinschi
 */
public interface Command {
    public void execute();
}
